<?php
/*
データに使えるラベルが変数である
文字列がstring
integerが浮動小数
論理値 boolean
配列
オブジェクト
null
データ型を意識する必要はない
定数の利用をする

定数を使用する
//定数はプログラム上で変更されないラベルである。

*/

// $msg = "hello from the top";
// echo $msg;
// var_dump($msg);

/*lesson5
define("MY_EMAIL","chiyosato2001@gmail.com");

echo MY_EMAIL;
 // MY_EMAIL ="hogehoge";
var_dump(__LINE__);
var_dump(__FILE__);
var_dump(__DIR__);
*/

/* lesson6
数値型の演算子
** べき乗
*/
// $x = 10 % 3; //1
// $y = 30.2/4; //4
// echo $x;
// var_dump($y);

//単項演算子
// $z = 5;
// $z++;//6
// var_dump($z);

//代入を伴う演算子
// $x = 5;
// $x = $x * 2;
// $x *= 2; //1-
// var_dump($x);

//文字列を扱おう
// "" 特殊文字や変数
// ''

// $name = "satoshi";
// $s1 = "hello $name!\n hello again!";
// $s1 = "hello {$name}!\n hello again!";
// $s1 = "hello ${name}!\n hello again!";
// $s2 = 'hello $name!\n hello again!';
// var_dump($s1);
// var_dump($s2);

//文字列の連結
// $s = "hello"."world";
// var_dump($s);

// if 条件分岐
//比較演算子 > >= <= == === != !==
//論理演算子 and && , or || , !

// $score = 80;

// if($score > 80){
// 	echo "great!";
// 	}elseif($score > 60){
// 		echo "good";
// 	}else{
// 		echo "so so ...";
// }

//真偽値について理解を深めよう
/*真偽値を調べたい
空文字列またはゼロの文字れる
数値は0 0.0
配列の要素の数がゼロ
nullはゼロになる
*/

// $x = 5;
// // if($x){
// if($x == true){
// 	echo "great!";
// }

//三項演算子 if文の置き換えです
// $max = ($a > $b) ? $a : $b;

// if($a > $b){
// 	$max = $a;
// 	} else {
// 		$max = $b;
// 	}

//switch文による条件分岐

// $signal = "green";

// switch ($signal) {
// 	case "red":
// 		echo "stop!";
// 		break;

// 	case "yellow":
// 		echo "be aware!";
// 		break;

// 	case "blue":
// 	case "green":
// 		echo "go";
// 		break;

// 	default:
// 		echo "no signal";
// 		break;
// }


//ループ処理
//do while文を書く

// $i = 100;
// while($i < 10){
// 	echo $i;
// 	$i++;
// }

// do {
// 	echo $i;
// 	$i++;
// 	} while ($i < 10);


// for で継続する
// break
// continue 実行せずにつぎのループに移ります。


// for($i = 0; $i< 10; $i++){
// 	if($i ===5){
// 		// break;
// 		continue;
// 	}
// 	echo $i;
// }

//配列の利用をする
//key value

// $sales = array(
//  "satoshi" => 100,
//  "yamada" => 200,
//  "namida" => 300,
// );

// $family = [
//  "satoshi" => "papa",
//  "yuko" => "mama",
//  "natsuko" => "daughter",
// ];

// // print_r($sales);
// var_dump($sales["satoshi"]); //100
// $sales["satoshi"] = 1000;
// var_dump($sales["satoshi"]); //900

//$colors =["red","blue","pink"];
// var_dump($colors[1]); //blue

// for each

// foreach ($sales as $key => $value) {
// 	echo "($key) $value";
// }


// foreach ($colors as $value) {
// 	echo "$value";
// }

//if while for コロン構文


//関数

// function sayHi($name = "satoshi"){
// 	echo "hi!".$name;
// 	return "hi!".$name;
// }

// // sayHi();

// // sayHi("Tom");
// // sayHi("Bob");
// // sayHi();

// $s = sayHi();
// var_dump($s);


//ローカル変数

// $lang = "ruby";

// function sayHi($name){
// 	$lang = "php";
// 	echo "hi!".$name;
// }

// sayHi("Tom");
// var_dump($lang);


//組み込み関数

// $x = 5.6;
// echo ceil($x); //6
// echo floor($x);
// echo round($x);
// echo rand(1,10);


// $s1 = "hello";
// $s2 = "ねこ";
// echo strlen($s1);
// echo mb_strlen($s2);
// printf("%s - %s - %.3f",$s1,$s2,$x);

// $colors =["red","blue","pink"];

// echo count($colors);
// echo implode("@",$colors);

//class instance method

//class property method

//class instance

// class User{
// 	public $name;
// 	public function __construct($name){
// 		$this->name = $name;
// 	}

// 	public function sayHi(){
// 		echo "hi, I am $this->name!";
// 	}
// }


// class AdminUser extends User{
// 	public function sayHello(){
// 		echo "hello from admin!";
//  	}
//  	//override
//  	public function sayHi(){
//  		echo "hi,[admin] I am $this->name!";
//  	}
//  }

// 	$tom = new User("Tom");
// 	$steve = new AdminUser("Steve");
// 	// echo $steve->name;
// 	$steve->sayHi();
	// $steve->sayHello();

// $tom->sayHi();


//アクセス権について理解する

//private protected public
/*そのクラス内 親子クラス どこからでもOKです。
*/

// class User{
// 	// public $name;
// 	protected $name;
// 	public function __construct($name){
// 		$this->name = $name;
// 	}

// 	public function sayHi(){
// 		echo "hi, I am $this->name!";
// 	}
// }

// class AdminUser extends User{
// 	public function sayHello(){
// 		echo "hello from admin!";
//  	}
//  	//override
//  	public function sayHi(){
//  		echo "hi,I am $this->name!";
//  	}
//  }

// 	$tom = new User("Tom");
// 	// echo $tom->name;
// 	$steve = new AdminUser("Steve");
// 	$steve->sayHello();


//static

// class User{
// 	public $name;
// 	public static $count = 0;
// 	public function __construct($name){
// 		$this->name = $name;
// 		self::$count++;
// 	}
// 	public function sayHi(){
// 		echo "hi, i am $this->name!";
// 	}
// 	public static function getMessage(){
// 	echo "hello from User class!";
// 	}
// }
// 	// User::getMessage();
// 	$tom = new User("Tom");
// 	$bob = new User("Bob");

// echo User::$count; //2


//抽象クラスについて学ぶ

// abstract class BaseUser{
// 	public $name;
// 	abstract public function sayhi();
// }

//  class User extends BaseUser {
//  	public function sayhi() {
//  		echo "hello from user";
//  	}
//  }

//interface

// interface sayHi {
// 	public function sayHi();
// }

// interface sayHello{
// 	public function sayHello();
// }

// 	class User implements sayHi, sayHello{
// 		public function sayHi(){
// 			echo "hi!";
// 		}
// 		public function sayHello(){
// 			echo "Hello!";
// 		}
// 	}

//外部ファイルを読み込んでみよう

// require "User.class.php"; //エラーで終了
//require_once PHPがチェックしてくれる
//include ワーニングにする
//include_oncePHPがチェックしている
//autoload

// spl_autoload_register(function($class){
// 	require $class . ".class.php";
// });

// 	$bob = new User("Bob");
// 	$bob ->sayHi();

//なまえ空間

//クラスや関数がかぶらないようにする

// namespace Dotinstall\Lib;
// require "User.class.php";

// use Dotinstall\lib;

// $bob = new Lib\User("Bob");
// $bob->sayHi();


// function div($a, $b){
// 	echo $a / $b;
// }

// function div($a, $b){
// 	try{
// 		if($b === 0){
// 		throw new Exception("cannot divide by 0");
// 		}
// 	echo $a / $b;
// 		}catch(Exception $e){
// 			echo $e->getMessage();
// 		}
// 	}
// 	div(7,2);
// 	div(5,0);


//formからうけとる

 setcookie("userename","satoshi", time()+60*60);
 echo $_COOKIE['username'];


session_start();
$_SESSION['username'] = "satoshi";
echo $_SESSION['username'];
// unset($_SESSION['username']);




